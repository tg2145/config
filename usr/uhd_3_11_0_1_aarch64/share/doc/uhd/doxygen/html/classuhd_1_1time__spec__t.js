var classuhd_1_1time__spec__t =
[
    [ "time_spec_t", "classuhd_1_1time__spec__t.html#a2bd6c4c8ad7209d219c3ca5ae238fb6e", null ],
    [ "time_spec_t", "classuhd_1_1time__spec__t.html#a37e2cc38aa479200989cd3c7c1e4814b", null ],
    [ "time_spec_t", "classuhd_1_1time__spec__t.html#a8c8aee70db00810c0914fab28dfdc645", null ],
    [ "get_frac_secs", "classuhd_1_1time__spec__t.html#a5036d38625edd7e7669cd9379a9c8695", null ],
    [ "get_full_secs", "classuhd_1_1time__spec__t.html#a676a23d1521d83be97db9793300c05ad", null ],
    [ "get_real_secs", "classuhd_1_1time__spec__t.html#a6ba38d752eeb3de17a3a3589a0b0decf", null ],
    [ "get_tick_count", "classuhd_1_1time__spec__t.html#a24027fdefa9dd374f7c2c2d7fc0da26d", null ],
    [ "operator+=", "classuhd_1_1time__spec__t.html#aee41ca079ce9d090c801c0ce08dd2c36", null ],
    [ "operator-=", "classuhd_1_1time__spec__t.html#af0a274e20c8576ac8e5b1b2cbf0982d6", null ],
    [ "to_ticks", "classuhd_1_1time__spec__t.html#ae46f6c776d85de986769f29540a546e8", null ]
];