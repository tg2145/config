var NAVTREE =
[
  [ "USRP Hardware Driver and USRP Manual", "index.html", [
    [ "Table Of Contents", "index.html", "index" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"algorithm_8hpp.html",
"classuhd_1_1math_1_1fp__compare_1_1fp__compare__epsilon.html#a8f841b7afbda9cccbba82d204cc859d1",
"classuhd_1_1transport_1_1usb__control.html",
"classuhd_1_1usrp_1_1multi__usrp.html#aee040da1b7ae375e0c08bb0b080d7ccc",
"functions_func_m.html",
"namespaceuhd_1_1cal.html",
"page_rdtesting.html#rdtesting_phase_rx_manual",
"page_usrp_x3x0.html#x3x0_hw_1gige",
"structuhd_1_1fs__path.html#a678ecd86b586c09a2b54a76e1df6aa5b",
"structuhd_1_1usrp_1_1component__file__t.html#a393d2aeca20899b3fc933267905d843a",
"usrp_8h.html#ad08c7146dd1c185c0651fe02724c7a7c"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';